#!/bin/sh

for name in *; do
    imv "$name" &
    pid_imv=$!
    # TODO: Edit only tags
    new_name=$(dialog --stdout --title "${name}" --inputbox "Rename file:" 0 100 "${name}")
    if [ ! -f "${new_name}" ]; then
        # TODO: Add special suffix 
        mv "${name}" "${new_name}"
    fi
    kill ${pid_imv}
done
