#!/bin/sh

name="$1"

exif_metadata(){
    modify=$(exiftool -dateTimeOriginal "$name" | grep -Eow "([0-9][0-9:]+) ([0-9:]+)")

    if [ -z "${modify}" ];then
        echo "Empty modfication time [$name]"
    else
        date=$(echo "$modify" | cut -f1 -d' ')
        time=$(echo "$modify" | cut -f2 -d' ')

        if [ -z "${date}" ] && [ -z "${time}" ];then
            echo "Invalid date or time [$name]"
        else
            date_formatted=$(echo "${date}" | sed 's/:/-/g')
            time_formatted=$(echo "${time}" | sed 's/:/./g')
            prefix="${date_formatted}T${time_formatted}"    
            sanitized_name=$(echo "${name}" | sed -e "s/^[0-9-]\+T[0-9.]\+//g")
            new_name=${prefix}${sanitized_name}

            if [ "${name#*$prefix}" != "$name" ];then
                 :
            else
                echo "$name -> $new_name"
                mv "$name" "$new_name"
            fi
        fi 
    fi
}

ffprobe_metadata(){

    modify=$(ffprobe -v quiet -select_streams v:0 -show_entries stream_tags=creation_time -of default=noprint_wrappers=1:nokey=1 "$name" | grep -Eow "[0-9\-]+T[0-9:]+" | sed 's/:/./g'
}

exif_metadata
ffprobe_metadata
